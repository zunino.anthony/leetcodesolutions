# LeetCodeSolutions

This is a project in development started in 3/19/24 by **Anthony Zunino**. It contains all the practice solutions from LeetCode problems. My LeetCode account can be found [here](https://leetcode.com/tzseadog/).

## Name
LeetCodeSolutions

## Description
A simple repo containing the solutions to various code problems. Many of the questions derive from [LeetCode 75](https://leetcode.com/studyplan/leetcode-75/).

**To simplify and speed up programming, all problems will lie in the same file until I have done enough problems to split into separate files**
